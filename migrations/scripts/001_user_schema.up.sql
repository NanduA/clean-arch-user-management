CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY,
    email VARCHAR(31),
    password VARCHAR(15),
    name VARCHAR(31),
    gender VARCHAR(15),
    dob DATE,
    created_at TIMESTAMP,
    is_active BOOLEAN DEFAULT false
    is_deleted BOOLEAN DEFAULT false
);


