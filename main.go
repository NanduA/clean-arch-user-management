package main

import (
	"clean-code/migrations"
	"clean-code/server"
	"flag"

	"github.com/sirupsen/logrus"
)

var (
	migration = flag.Bool("migration", false, "This is a string argument for running migration")
	up        = flag.Bool("up", false, "This is a string argument for running migration up")
	down      = flag.Bool("down", false, "This is a string argument for running migration down")
)

func main() {
	// logrus init
	log := logrus.New()

	flag.Parse()

	// Database migration
	if *migration {
		if !*down && !*up {
			log.Fatal("Please specify the migration type")
		}
		if *up {
			migrations.Migration("up")
		}
		if *down {
			migrations.Migration("down")
		}
	} else {
		server.Run()
	}
}
