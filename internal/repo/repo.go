package repo

import (
	"clean-code/internal/entities"
	"database/sql"
)

// uSERserRepo struct
type UserRepo struct {
	db  *sql.DB
	env *entities.EnvConfig
}

// UserRepoImply is an interface for the User repository implementation.
type UserRepoImply interface {
}

// NewUserserRepo creates a new instance of UserRepo.
func NewUserRepo(db *sql.DB, env *entities.EnvConfig) UserRepoImply {
	return &UserRepo{db: db, env: env}
}
