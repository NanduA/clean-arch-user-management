package controllers

import (
	"clean-code/internal/usecases"
	"clean-code/version"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UserController struct
type UserController struct {
	router   *gin.RouterGroup
	useCases usecases.UserUseCaseImply
}

// NewUserControllers creates controller struct
func NewUserControllers(router *gin.RouterGroup, appUseCase usecases.UserUseCaseImply) *UserController {
	return &UserController{
		router:   router,
		useCases: appUseCase,
	}
}

// InitRoutes intialise the routes
func (app *UserController) InitRoutes() {

	// Health handler
	app.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, app, "HealthHandler")
	})

}

// HealthHandler check api health
func (app *UserController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}
