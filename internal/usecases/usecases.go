package usecases

import (
	"clean-code/internal/repo"
)

// UserUseCases represents the use cases for User-related operations.
type UserUseCases struct {
	repo repo.UserRepoImply
}

// UserUseCaseImply is an interface for the User use case implementation.
type UserUseCaseImply interface {
}

// NewUserUseCases creates a new instance of UserUseCases.
func NewUserUsecases(UserRepo repo.UserRepoImply) UserUseCaseImply {
	return &UserUseCases{
		repo: UserRepo,
	}
}
