package consts

// API versioning
const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)

// common constants
const (
	DatabaseType     = "postgres"
	AppName          = "sampleproject"
	AcceptedVersions = "v1"
)
