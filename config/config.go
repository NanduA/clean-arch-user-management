package config

import (
	"clean-code/internal/entities"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

// LoadConfig to load environment variables
func LoadConfig(appName string) (*entities.EnvConfig, error) {
	// Load environment variables from the .env file
	err := godotenv.Load()
	if err != nil {
		return nil, err
	}
	// Bind environment variables to a struct variable
	var cfg entities.EnvConfig
	err = envconfig.Process(appName, &cfg)
	if err != nil {
		return nil, err
	}
	return &cfg, nil
}
