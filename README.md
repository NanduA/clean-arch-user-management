# Beer Tap Dispenser API Documentation

## Overview
The Beer Tap Dispenser API allows you to manage dispensers, their statuses, and related information.

## Base URL
The base URL for all API endpoints is: `http://example.com/api/:version`


## Endpoints

### 1. Health Check
- **Endpoint:** `GET /api/:version/health`
- **Description:** Perform a health check to verify that the API is up and running.
- **Response:**
    - Status Code: 200 (OK)
    - Content: `{ "status": "healthy" }`

### 2. Create a Dispenser
- **Endpoint:** `POST /api/:version/dispensers`
- **Description:** Create a new dispenser with flow volume information.
- **Request Body:**
    - Example:
        ```json
        {
          "flow_volume": 0.05
        }
        ```
- **Response:**
    - Status Code: 200 (OK) - If the dispenser is created successfully.
    - Status Code: 400 (Bad Request) - If there are validation errors or missing data in the request.
    - Status Code: 500 (Internal Server Error) - If there's an internal error during the creation process.

### 3. Get All Dispensers
- **Endpoint:** `GET /api/:version/dispensers`
- **Description:** Retrieve a list of all dispensers with optional pagination and filtering parameters.
- **Query Parameters:**
    - `limit`: (Optional) The maximum number of dispensers to return per page (default: 10).
    - `offset`: (Optional) The page number for pagination (default: 0).
    - `status`: (Optional) Filter dispensers by status (e.g., "open", "closed").
- **Response:**
    - Status Code: 200 (OK) - If the request is successful.
    - Status Code: 500 (Internal Server Error) - If there's an internal error during the retrieval process.
- **Response Body:**
    - Example:
        ```json
        {
          "data": [
            {
              "id": "c6a20e57-4ab6-4819-85cd-9c7fc187ae60",
              "flow_volume": 100.5,
              "status": "open",
              "opened_at": "2023-08-01T12:34:56Z",
              "closed_at": null,
              "total_amount": 0.0
            },
            {
              "id": "f12b486a-99b6-4e3e-b657-9128f070942c",
              "flow_volume": 80.0,
              "status": "closed",
              "opened_at": "2023-08-01T10:00:00Z",
              "closed_at": "2023-08-01T12:30:00Z",
              "total_amount": 50.0
            }
          ],
          "meta_data": {
            "total": 2,
            "per_page": 10,
            "current_page": 1,
            "next": 0,
            "prev": 0
          }
        }
        ```

### 4. Get a Dispenser by ID
- **Endpoint:** `GET /api/:version/dispensers/:dispenser_id`
- **Description:** Retrieve information about a specific dispenser by its ID.
- **Response:**
    - Status Code: 200 (OK) - If the request is successful.
    - Status Code: 404 (Not Found) - If the dispenser with the given ID is not found.
    - Status Code: 500 (Internal Server Error) - If there's an internal error during the retrieval process.
- **Response Body:**
    - Example:
        ```json
        {
          "id": "c6a20e57-4ab6-4819-85cd-9c7fc187ae60",
          "flow_volume": 100.5,
          "status": "open",
          "opened_at": "2023-08-01T12:34:56Z",
          "closed_at": null,
          "total_amount": 0.0
        }
        ```

### 5. Update a Dispenser
- **Endpoint:** `PUT /api/:version/dispensers/:dispenser_id`
- **Description:** Update the status of a specific dispenser by its ID.
- **Request Body:**
    - Example:
        ```json
        {
          "status": "closed"
        }
        ```
- **Response:**
    - Status Code: 200 (OK) - If the update is successful.
    - Status Code: 400 (Bad Request) - If there are validation errors or missing data in the request.
    - Status Code: 404 (Not Found) - If the dispenser with the given ID is not found.
    - Status Code: 500 (Internal Server Error) - If there's an internal error during the update process.

### 6. Delete a Dispenser
- **Endpoint:** `DELETE /api/:version/dispensers/:dispenser_id`
- **Description:** Delete a specific dispenser by its ID.
- **Response:**
    - Status Code: 200 (OK) - If the delete is successful.
    - Status Code: 404 (Not Found) - If the dispenser with the given ID is not found.
    - Status Code: 409 (Conflict) - If the dispenser is currently in use and cannot be deleted.
    - Status Code: 500 (Internal Server Error) - If there's an internal error during the deletion process.

Please note that you should replace `:version` in the endpoints with the appropriate API version. Additionally, ensure you have proper authentication and authorization mechanisms in place for secure API access.

## How to run the code

    go run main.go

    or

    docker compose up